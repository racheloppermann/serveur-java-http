package packageHTTP;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Rachel
 * Réponse HTTP
 */
public class HttpResponse {

	public static final String version = "HTTP/1.0";

	List<String> headers = new ArrayList<String>();

	byte[] body;

	/**
	 * Constructeur
	 * @param requete
	 * @throws IOException
	 */
	public HttpResponse(HttpRequest requete) throws IOException {

		switch (requete.methode) {
			
			case GET:
				affichage(requete);
				break;
			case POST:
				affichage(requete);
				break;
			case NONCONNU:
				headers(Code._405);
				reponse(Code._405.toString());
				break;
			default:
				headers(Code._405);
				reponse(Code._405.toString());
		}

	}
	
	/**
	 * Affichage de la réponse 
	 * 3 parties : Sans fichier, s'il y a un fichier ou si un fichier n'est pas trouvé.
	 * @param requete
	 */
	private void affichage(HttpRequest requete) {
		List<String> headersAffichage = new ArrayList<String>();
		
		try {
			File fichier = new File("." + requete.uri);
			if (fichier.isDirectory()) {
			    headers(Code._200);   
			    
				headers.add(TypeContenu.HTML.toString());
				headersAffichage.addAll(headers);	
				StringBuilder resultat = new StringBuilder
						("<html><body>Index (Si vous voulez afficher un fichier, saisir /files/ + le nom du fichier ");
				resultat.append(formatHeadersHTTP(headersAffichage));
				resultat.append("</body></html>");
				reponse(resultat.toString());

			} else if (fichier.exists()) {
			    headers(Code._200);
				setTypeContenu(requete.uri, headers);
				headersAffichage.addAll(headers);	
				//content length assigné automatiquement, le mettre à la main cause des erreurs
				headersAffichage.add("Content-Length: " + (int) fichier.length());
				byte[] both = join(formatHeadersHTTP(headersAffichage).getBytes(), getBytes(fichier));
				if(setTypeContenu(requete.uri, headers).contains("image")) {
					reponse(getBytes(fichier));
				}
				else {
					reponse(both);
				}
						
			} else {
				System.out.println("File not found:" + requete.uri);
				headers(Code._404);
				headersAffichage.addAll(headers);	
				reponse(formatHeaders(headersAffichage));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			headers(Code._400);
			reponse(Code._400.toString());
		}
	}

	/**
	 * Regroupement de 2 données en byte a et b pour un retour commun c
	 * @param a
	 * @param b
	 * @return
	 */
	private byte[] join(byte[] a, byte[] b) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
		try {
			outputStream.write( a );
			outputStream.write( b );
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		byte c[] = outputStream.toByteArray( );
		
		return c;
	}

	/**
	 * on récupère les bytes du fichier
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private byte[] getBytes(File file) throws IOException {
		int length = (int) file.length();
		byte[] array = new byte[length];
		InputStream in = new FileInputStream(file);
		int offset = 0;
		while (offset < length) {
			int count = in.read(array, offset, (length - offset));
			offset += count;
		}
		in.close();
		return array;
	}
	
	/**
	 * Affichage de la partie Header format html
	 * @param headers
	 * @return
	 */
	private String formatHeadersHTTP(List<String> headers) {
		String resultat = "";
		for(String header : headers) {
			resultat = resultat + "<p>" + header + "</p>" + "\n";	
		}
		return resultat + "\n";
	}
	
	/**
	 * Affichage de la partie Header format normal
	 * @param headers
	 * @return
	 */
	private String formatHeaders(List<String> headers) {
		String resultat = "";
		for(String header : headers) {
			resultat = resultat + header + "\n";	
		}
		return resultat + "\n";
	}

	/**
	 * Ajout de diverses données à la liste d'headers
	 * @param status
	 */
	private void headers(Code status) {
		headers.add(HttpResponse.version + " " + status.toString());
		headers.add("Server: Serveur de Rachel");
		headers.add("Date: " + new Date().toString());
		headers.add("Connection: close");
	}

	/**
	 * Réponse des requête de type string
	 * @param reponse
	 */
	private void reponse(String reponse) {
		body = reponse.getBytes();
	}

	/**
	 * Réponse des requêtes de types byte
	 * @param reponse
	 */
	private void reponse(byte[] reponse) {
		body = reponse;
	}

	/**
	 * Ecriture de la réponse
	 * @param os
	 * @throws IOException
	 */
	public void ecrire(OutputStream os) throws IOException {
		DataOutputStream output = new DataOutputStream(os);
		for (String header : headers) {
			output.writeBytes(header + "\r\n");
		}
		output.writeBytes("\r\n");
		if (body != null) {
			output.write(body);
		}
		output.writeBytes("\r\n");
		output.flush();
	}

	/**
	 * On détermine le type de contenu du fichier
	 * @param uri
	 * @param list
	 * @param type 
	 */
	private String setTypeContenu(String uri, List<String> list) {
		String ext = uri.substring(uri.indexOf(".") + 1);
		TypeContenu type = TypeContenu.valueOf(ext.toUpperCase());
		try {
			list.add(type.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return type.toString();
	}
}
