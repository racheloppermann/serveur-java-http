package packageHTTP;

/**
 * @author Rachel
 * Méthodes GET ou POST
 */
public enum Methode {
	GET("GET"), //
	POST("POST"), //
	NONCONNU(null); //

	private final String methode;

	Methode(String methode) {
		this.methode = methode;
	}
}
