package packageHTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rachel
 * Requête HTTP
 */
public class HttpRequest {

	List<String> headers = new ArrayList<String>();

	Methode methode;

	String uri;

	String version;

	/**
	 * Constructeur 
	 * @param is
	 * @throws IOException
	 */
	public HttpRequest(InputStream is) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String string = reader.readLine();
		parseRequete(string);

		while (!string.equals("")) {
			string = reader.readLine();
			parseRequeteHeader(string);
		}
	}

	/**
	 * On récupère la donnée d'entrée et on la split dans uri et version
	 * @param string
	 */
	private void parseRequete(String string) {
		System.out.println(string);
		String[] split = string.split("\\s+");
		try {
			methode = Methode.valueOf(split[0]);
		} catch (Exception e) {
			methode = Methode.NONCONNU;
		}
		uri = split[1];
		version = split[2];
	}

	/**
	 * On ajoute string à la liste de headers
	 * @param string
	 */
	private void parseRequeteHeader(String string) {
		System.out.println(string);
		headers.add(string);
	}
}
