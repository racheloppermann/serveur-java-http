package packageHTTP;

/**
 * @author Rachel
 * Type de contenu des fichiers
 */
public enum TypeContenu {
	HTM("HTM"), //
	HTML("HTML"), //
	CSS("CSS"), //
	CSV("CSV"), //
	GIF("GIF"), //
	JAR("JAR"), //
	JPEG("JPEG"), //
	JPG("JPG"), //
	JS("JS"), //
	JSON("JSON"), //
	PNG("PNG"), //
	PDF("PDF"), //
	SVG("SVG"); //

	private final String type;

	TypeContenu(String type) {
		this.type = type;
	}

	/**
	 * Affichage du content type
	 */
	@Override
	public String toString() {
		
		switch (this) {
			case HTM:
			case HTML:
				return "Content-Type: text/html";
			case CSS:
				return "Content-Type: text/css";
			case CSV:
				return "Content-Type: text/csv";
			case GIF:
				return "Content-Type: image/gif";
			case JAR:
				return "Content-Type: application/java-archive";
			case JPEG:
			case JPG:
				return "Content-Type: image/jpeg";
			case JS:
				return "Content-Type: application/javascript";
			case JSON:
				return "Content-Type: application/json";
			case PNG:
				return "Content-Type: image/png";
			case PDF:
				return "Content-Type: application/pdf";
			case SVG:
				return "Content-Type: image/svg+xml";
			default:
				return null;
		}
	}
}
