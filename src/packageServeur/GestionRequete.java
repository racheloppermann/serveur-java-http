package packageServeur;

import java.net.Socket;

import packageHTTP.HttpRequest;
import packageHTTP.HttpResponse;

/**
 * @author Rachel
 * Gestion de la requête HTTP
 */
public class GestionRequete implements Runnable {


	private Socket socket;

	/**
	 * Constructeur
	 * @param socket
	 */
	public GestionRequete(Socket socket) {
		this.socket = socket;
	}

	/**
	 * Méthode run
	 * On crée une nouvelle requête et reponse quand l'utilisateur se connecte
	 */
	public void run() {
		try {
			HttpRequest requete = new HttpRequest(socket.getInputStream());
			HttpResponse reponse = new HttpResponse(requete);
			reponse.ecrire(socket.getOutputStream());
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
