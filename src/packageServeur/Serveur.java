package packageServeur;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Rachel
 * Serveur
 */
public class Serveur {

	private static final int port = 8080;

	private static final int nbThread = 5;

	/**
	 * Main
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			new Serveur().start(getPortValide(args));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Création du serveur et connexion à celui-ci
	 * @param port
	 * @throws IOException
	 */
	public void start(int port) throws IOException {
		ServerSocket serveur = new ServerSocket(port);
		System.out.println("Le serveur attend la connexion sur le port " + port);
		ExecutorService executor = Executors.newFixedThreadPool(nbThread);
		while (true) {
			executor.submit(new GestionRequete(serveur.accept()));
		}
	}

	/**
	 * On vérifie le port saisi par l'utilisateur
	 * @param args
	 * @return
	 * @throws NumberFormatException
	 */
	static int getPortValide(String args[]) throws NumberFormatException {
		if (args.length > 0) {
			int port = Integer.parseInt(args[0]);
			if (port > 0 && port < 65535) {
				return port;
			} else {
				throw new NumberFormatException("Port invalide. La valeur du port est un nombre entre 0 et 65535");
			}
		}
		return port;
	}
}
